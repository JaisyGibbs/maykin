from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("cities/<int:city_id>", views.city, name="city.show"),
    path("cities/<int:city_id>/hotels", views.city_hotels, name="city.hotels"),
    path("import_csv", views.import_csv, name="import_csv"),
]