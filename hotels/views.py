from django.shortcuts import render
from django.http import HttpResponse
from django.core.management import call_command
from .models import City
from django.template import loader

def index(request):
    cities = City.objects.all()

    context = {
        "cities": cities,
    }

    return render(request, 'index.html', context)

def city(request, city_id):
    city = City.objects.get(pk=city_id)

    context = {
        "city": city,
        "hotels": city.hotel_set.all(),
    }

    return render(request, 'city.html', context)

# Get all hotels of a specific city
def city_hotels(request, city_id):
    city = City.objects.get(pk=city_id)

    context = {
        "city": city,
        "hotels": city.hotel_set.all(),
    }

    return render(request, 'hotels/table.html', context)

# Execute the command to import the CSV file
def import_csv(request):
    call_command('ImportCitiesAndHotels')
    return HttpResponse("Imported cities and hotels")
