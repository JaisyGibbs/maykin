from django.db import models

# Create your models here.
class City(models.Model):
    name = models.CharField(max_length=255)
    city_code = models.CharField(max_length=3)

    def __str__(self):
        return self.name

class Hotel(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    hotel_code = models.CharField(max_length=5)

    def __str__(self):
        return self.name