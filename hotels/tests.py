from django.test import TestCase
from django.core.management import call_command
from .models import City, Hotel

class ImportTests(TestCase):
    def setUp(self):
        """
        Setup the test
        """
        call_command('ImportCitiesAndHotels')

    def test_import_suceeds(self):
        """
        Test if the import command works as expected
        """
        self.assertGreater(City.objects.count(), 0)
        self.assertGreater(Hotel.objects.count(), 0)
                      
    def test_city_has_many_hotels(self):
        """
        Test if the city has many hotels
        """
        city = City.objects.first()
        self.assertGreater(city.hotel_set.count(), 0)

    def test_hotel_belongs_to_city(self):
        """
        Test if the hotel has a city
        """
        hotel = Hotel.objects.first()
        self.assertIsNotNone(hotel.city)

class ViewTests(TestCase):
    def test_index_without_cities(self):
        """
        Test if the index with no cities
        """
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No cities found")

    def test_index_with_cities(self):
        """
        Test if the cities are displayed on the index page
        """
        call_command('ImportCitiesAndHotels')
        response = self.client.get('/')
        self.assertGreater(len(response.context['cities']), 0)