from django.core.management.base import BaseCommand
from hotels.models import City, Hotel
import requests
from requests.auth import HTTPBasicAuth
import csv
from io import StringIO

class Command(BaseCommand):
    help = "Import cities and hotels from a remote CSV file"

    def handle(self, *args, **options):
        # Delete all cities and hotels, so we can start with a clean slate
        # We also clear the database, so we do not have any duplicate cities or hotels
        # This could be fixed by checking if the city or hotel already exists, but for development purposes this is will do
        City.objects.all().delete()
        Hotel.objects.all().delete()

        httpAuth = HTTPBasicAuth('python-demo', 'claw30_bumps')

        self.import_cities(httpAuth)
        self.import_hotels(httpAuth)
    
    def import_cities(self, httpAuth):
        cities_url = "http://rachel.maykinmedia.nl/djangocase/city.csv"
        data = self.load_csv(cities_url, httpAuth)

        # Now that we have imported the csv data, we can create the hotels
        for row in data:
            city_code, city_name = row[0].split(";")
            City.objects.create(name=city_name.strip('"'), city_code=city_code)

    def import_hotels(self, httpAuth):
        hotels_url = "http://rachel.maykinmedia.nl/djangocase/hotel.csv"
        csv_reader = self.load_csv(hotels_url, httpAuth)

        # Now that we have imported the csv data, we can create the hotels
        for row in csv_reader:
            city_code, hotel_code, hotel_name  = row[0].split(";")
            city = City.objects.filter(city_code=city_code).first() # Get the city object
            Hotel.objects.create(name=hotel_name.strip('"'), hotel_code=hotel_code.strip('"'), city=city)

    # Load Csv data from given url, since the url is protected with basic auth, we need to pass the auth object
    def load_csv(self, url, auth):
        response = requests.get(url, auth=auth)
        csv_data = StringIO(response.text)
        data = csv.reader(csv_data)
        return data
